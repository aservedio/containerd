#!make

MAKEFLAGS = --no-print-directory

CI_REGISTRY_IMAGE ?= dev-containerd
TAG ?= dev

all: build

.PHONY: build
build:
	docker build \
		--file build/Dockerfile \
		-t ${CI_REGISTRY_IMAGE}:${TAG} \
		build/
	@echo "Built image:"
	@echo "  ${CI_REGISTRY_IMAGE}:${TAG}"

.PHONY: shell
shell:
	docker run --rm -it ${CI_REGISTRY_IMAGE}:${TAG}

.PHONY: build-push
build-push: build
build-push:
	docker push ${CI_REGISTRY_IMAGE}:${TAG}

.PHONY: shell-special
shell-special:
	docker run --rm -it --entrypoint '' --privileged -v /tmp:/tmp --tmpfs /workspaces/containerd/containerd-test ${CI_REGISTRY_IMAGE}:${TAG} bash
